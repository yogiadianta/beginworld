# Go SQL Struggle

## What I don't like about go-pg


This count syntax:
```
count, err := db.Model((*models.Player)(nil)).Count()

db.Model(&models.Player{}).Count(&count)
```

```
// Go-PG
_, err := db.Model(&u).
  Where("name = ?name").
  OnConflict("DO NOTHING").
  SelectOrInsert()

// GORM
db = db.Clauses(clause.OnConflict{DoNothing: true}).Create(&u)
```

## 2 Counts in GORM

Two Ways to accomplish the same thang
```
db.Model(&ColorschemeVote{}).Where("theme = ?", theme).Count(&count)

db.Table("colorscheme_votes").Where("theme = ?", theme).Count(&count)
```

Difference Between Save and Create:
  -> Save requires a ID

## Resources

- https://go-database-sql.org/
- https://github.com/lib/pq


- pq is exclusively used with database/sql.
- go-pg does not use database/sql at all.
- pgx supports database/sql as well as its own interface.
  - https://github.com/open-telemetry/opentelemetry-go-contrib/issues/5

- How do we make it possible to easily change DB drivers

It is recommended to use the pgx interface if:
  - The application only targets PostgreSQL.
  - No other libraries that require database/sql are in use.

- pq versus pgx

- should we switch from go-pg, to sqlx
  - go-pg -> that that Go-like. Idiomatic Go???
  - sqlx IS NOT AN ORM
  - I'm one reason away from switching
  - Work is using go-pg, but I ain't like it.
    - I will eventually match wor
  - Gorm for migrations

Hashicorp Boundary:
  - pq + Gorm

https://github.com/lib/pq
go-pg:
  - ORM
  - DB Driver
  - Migration support
OR:
  - pgx -> DB Driver
  - sqlx -> Kinda the ORM (not really, but he Marshal and prepared statements help)
    - Marshal rows into structs (with embedded struct support), maps, and slices
    - Named parameter support including prepared statements
    - Get and Select to go quickly from query to struct/slice
  - gorm -> migrations

  - https://github.com/gobuffaloio/pop

is go-pg: ORM and Sql Driver???

Just driver???
https://github.com/jackc/pgx

