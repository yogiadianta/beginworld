## BeginWorld merges with Factorio

## Goals

## Problems

- How can we run headless with mods
  - im guessing pass the mod folder to the factorio --start-server command

- How can I let the Steam-connected player run admin commands?

- What is the proper chat to interact with the console:
  - player

## Setup

RCON:
  - 1.267 Info RemoteCommandProcessor.cpp:131: Starting RCON interface at IP ADDR:({0.0.0.0:27015})

Headless Server:
  - 0.0.0.0:34197

### Starting Headless Server

```
factorio --start-server ~/.factorio/saves/base_mod.zip --rcon-port 27016 --rcon-password password -c ~/.factorio/config/server_config.ini --mod-directory ~/.factorio/mods
```

```
--mod-directory PATH      Mod directory to use
```

### From TwitchChat

```
!game.player.teleport(100, 100)

$player.surface.create_entity({name='crude-oil', amount=100000, position={player.position.x, player.position.y}})
```

```
~/code/factorio_mods/TwitchToFactorio
```

