# Onyx Pupper

## Goals

- Web App Runing Locally and on the Web
- Screen Scrapping some Sites
- Saving info into a DB
- Present some aggregated info on the data scrapped

<leader>s
1z=


## Tech

- DB tech
	-> What Database: SQL-like
		- Postgresql***, MariaDB, MySql
	-> Where are you deploying this DB
	-> How are you Modeling your data

- Language
	- Python
	- Framework???
		- Django
		- Flask
		- Pyramid
		- aiohttp
 		- fastapi

- Webscrapping
	- Beautiful Soup

- Whats the UI built in?
	- Is there JS involved?
	- This just a backend

- Deploy: Where are we deploying???
	- Heroku
	- Netlify
	- Linode
	- Vultr
  - Vercel

---

Don't Learn SQlite as your first DB, because when you need to deploy, you often
have to change, and the startup learning time for Postgres and SQLite its the same.
Learning SQLite, then having to switch when you get to deploy,
can be annoying!!!

---

Project Idea:

• Search engine to search multiple sites
    • Be able to extract information from amazon/bodybuilding.com/gnc/nutrition zone
    • Database to hold information and sort with tables

    • Sub table
    • Diversify ingredients
    • Recommended bundles on front page ranging from cheapest to most expensive

    • Able to scan websites and search for best pricing/ review’s
    • Sorting between vegan/keto/ plant-based items
    • Stackable components to cut down on redundancy and overspending i.e. compatibility
    • Prebuilt: for endurance athletes
    • Strength athletes
    • Bulking/cutting weight
    • Ability to filter double posting
    • Guides describing levels of needs for supplements
    • Pulled from forums and NASM database
    • Have user-based stacks as well with best reviews based on analytics from sales
    • Returning customer priority review over first time
    • Health related section
    • Bad health: certain proteins for gastric bypass patients
    • Low testosterone supplements and virility
    • Good health: gingko biloba (memory),
    • Beauty products
    • Hyaluronic acid products
    • Biotin
