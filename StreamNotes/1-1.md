# 1-1

!hide nightbegin

:help terminal-input


Input						*terminal-input*

To send input, enter |Terminal-mode| using any command that would enter "insert
mode" in a normal buffer, such as |i| or |:startinsert|. In this mode all keys
except <C-\><C-N> are sent to the underlying program. Use <C-\><C-N> to return
to normal-mode. |CTRL-\_CTRL-N|

Terminal-mode forces these local options:

    'nocursorline'
    'nocursorcolumn'
    'scrolloff' = 0
    'sidescrolloff' = 0

Terminal-mode has its own |:tnoremap| namespace for mappings, this can be used
to automate any terminal interaction.

To map <Esc> to exit terminal-mode: >
    :tnoremap <Esc> <C-\><C-n>

To simulate |i_CTRL-R| in terminal-mode: >
    :tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'

To use `ALT+{h,j,k,l}` to navigate windows from any mode: >
    :tnoremap <A-h> <C-\><C-N><C-w>h
    :tnoremap <A-j> <C-\><C-N><C-w>j
    :tnoremap <A-k> <C-\><C-N><C-w>k
    :tnoremap <A-l> <C-\><C-N><C-w>l
    :inoremap <A-h> <C-\><C-N><C-w>h
    :inoremap <A-j> <C-\><C-N><C-w>j
    :inoremap <A-k> <C-\><C-N><C-w>k
    :inoremap <A-l> <C-\><C-N><C-w>l
    :nnoremap <A-h> <C-w>h
    :nnoremap <A-j> <C-w>j
    :nnoremap <A-k> <C-w>k
    :nnoremap <A-l> <C-w>l

Mouse input has the following behavior:



READ THE DOCS DOESN'T ALWAYS WORK
DON'T BELEIVE THE LIES

...I read all of :help gf







































If you think VSCODE is easy.....I would suggest evalute
whether you are a Genius.

!slide carlvan

!save begid


































						*'errorfile'* *'ef'*
'errorfile' 'ef'	string	(default: "errors.err")
			global
	Name of the errorfile for the QuickFix mode (see |:cf|).
	When the "-q" command-line argument is used, 'errorfile' is set to the
	following argument.  See |-q|.
	NOT used for the ":make" command.  See 'makeef' for that.

!slide blunt

!restore_scene artmatt


'errorfile' 'ef'	string	(default: "errors.err")
			global
	Name of the errorfile for the QuickFix mode (see |:cf|).
	When the "-q" command-line argument is used, 'errorfile' is set to the
	following argument.  See |-q|.
	NOT used for the ":make" command.  See 'makeef' for that.
	Environment variables are expanded |:set_env|.
	See |option-backslash| about including spaces and backslashes.
	This option cannot be set from a |modeline| or in the |sandbox|, for
	security reasons.


- Use the Hyper and/or Meh as you general OBS trigger
- Set OBS to always listen for HotKeys
- Third -> OBS hotkey Menu is a Nightmare!
  -> you edit the JSON thoguh
  -> and remember is alphabetical order


## Cube as a Hobby

It is a cheap hobby if you do one type of 3x3
  -> one 60 dollar cube

## Streamer Confessions

- When I unlocked emotes I felt like I unlocked Homework,
  but eventually they formed naturally

## TODO Vim Day!

- Improve our Vim Workflow
- Convince Others to Join the Cult (Vim, Vi/Neovim, Tmux/Screen Linux Arch)
 Identify Bad Habit


## Arch Questions?

Do you have a good solution for playing videos hardware accelerated in the browser?

What videos?
Why?

mightycore1: Chromium is eating 20% of my CPU for playing twitch for example.
Because the GPU isn't used.

## Good Investment Advice

- Buy DS Chips and Shelf for later once they add CPU and GPUs to StockX

## January 1st

Busiest Day for:

  - NEW:
    - Streamers
    - Working Out
    - Writing
    - Meditating
