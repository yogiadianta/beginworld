# 1-18

## You Can Copy Code if you do it Right

boy1z

What is right:
  - rewrite it all by hand
  - comment annotate what everyline does
  - write down all the things you're confused about
  - look up those things
  - then delete all the code, and try and write it again yourself
  - if you have to look up the code, repeat

When you copy code, and as you are reading through it and understanding it,
you should updating it to match your domain.
Rename ya variables and ya functions to match the code you were writing before.

oskarallan: I like the boyscout method of using code, make sure you leave it better than you found it!

---

!slide alex
!moveup alex 100

!fall alex

!slide primeagen
!moveup primeagen 100

## Go's Learning Curve

### Begin's Opinion: Good first language

- Go is biased against abstractions
  - Just do it the obvious, simple way (sometimes this means more boiler-gate)
- The language is not huge
  - theres not a lot of syntax, its not endless tricks on tricks on methods etc.
- Standard library is full-featured, well commented, and easy to read the
  source code. If you want to learn Go, you can read the Go standard Library
  , I would NOT say the same for Ruby.
- GoByExample
- Lots of Go things, are standard language choices. You're not learning
  exotic syntax, you will see similar things in other langs
- It's fun to go fast with not that much effort

mdempsky: Still here.
mdempsky: If beginbot can program in Go, anyone can.

## EVERYONE WHO USES SOME RELATIONAL DB

- I love a heavily constrained DB,
  I like it when my Data  can't be saved if its bad
- But as things get complex, it makes test setup, more and more
  complicated.

What do you think about running your tests without all the DB constraints?

## Begin's Notices

- People who go right from sleep -> work,
  are always stressed out
  always complaining

## Custom Drop

f1ncc:

No person, no idea, and no religion deserves to be illegal to insult,
not even the Church of Emacs.

RMS

## Theories

- All the themes are not marked as themes in the DB
  - We are checking whether something is a theme,
    in 2 different ways;
    - theme flag on the stream_command table
    - does it match a player name

# Viewer Questions

profitztv: Top 3 tips for a new coder?

Top 3:
  - I. Take detailed notes on all your learning
    - Running journal of:
      - TIL: Today I learned
            - write down every small thing you learn
      - Confusion:
        - Whats confusing you, but you got to move on about
  - II. Avoid the Tutorial Loop of Doom:
    - Tutorials are to give you ideas to make you're own thing
    - If you just keep doing tutorials, you never develop
      the muscle to do things on your own, without instructions
    - As quickly as you can, start experimenting with your own
      code While doing the tutorial
  - III. Code some everyday, start early, give it focus.
    - Goal: 15 mins of fully focused learning/coding
        before you do anything "fun"

## Beginbotbot Issues

AllowUserAccessToCommand Error:

- beginbotbot chat messages aren't being read in
  - Might need new oath token

  ,,k

## OBS

!stats arby
!move arby

- Why is nomeme taking so long

!norm 922
!norm twitchchat
!buy all


## Claim to Fame

- First Twitch Partner that uses Arch Linux

- What is the shortest Partner to banning on Twitch so far

## Goal

- Improve the Meme Factory

## TODO

- Memes Page
  - Talk to Zanuss
- Moving of Memes
  - relatively

## OBS Problems

- Camera in OBS, keeps moving on restarting.
  - Need to save the settings in Beginworld

Theory: I'm on 1080 right now
...I think when I'm starting OBS its going to my 1080 Profile, 720 Profile.
I use the 1080 to record Youtube videos and 1080x720

moliendo: I guess you can configure the canvas in 1080 and scale to 720 to stream
moliendo: so you don't need 2 profiles
beginbot: I don't scale cuz it scares me

## Meme Synchronization

- Memes stored in Linode Object
- HTML -> references these Linode objects
- Postgresql Database
- The files stored Locally
  - OBS

## Kubernetes

mutrx: is kubernetes a meme? It seems most cost effective to host static files on S3, use a AWS RDS for db bc I don't trust myself to handle all that shit, and then just start with 1 EC2 and scale up eventually

Kubernetes IS A VERY USEFUL TECHNOLOGY IF YOU HAVE PROBLEMS THAT IT SOLVES

Don't use Kubernetes for everything
There is a problem of people using Kubernetes when they don't need to.
It's good to explore trying out the Kube in a Non-critical app,
just a lot of people then want to do for everything

Sometimes making overly complicated "solutions" is Job Security
People do this, and they may not even realize it.
I'm sure someone does it with impure intentions.
Often its just us dumb devs over complicated things,
because we are excited. You learn a new tech, and you want
to use everywhere.

Wait the static website is a Kubernetes.....better call Teej to make this
update.

....If you don't have people using your app....then why are thinking about it.


ltn_bob: Rather be scalable and not needed then need it and not have it

It costs money to make things "scalable"
Most things don't need to be "scalable"



----

## Resources

https://practicaltypography.com/ligatures-in-programming-fonts-hell-no.html
