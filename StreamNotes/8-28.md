# 2020-8-28

## DB Queries

- Are your Queries impacting customer queries
- Are you Querying against a reporting database
  - Some people replica DB, read only
    - to run "reporting against"
    - "Reporting" -> People running ad-hoc queries against the database
      - Ad-Hoc -> one off, made up on the fly
- Does the DB have indexes

## 3 Goals Everyday

- You brain must hurt from trying to understand something outside of your
  knowledge zone
- You have to embarrass yourself
  - we all care what people think
    - it's just what people
      - some keep a smallllll circle
- You must do something that scares you


---

lowfigamer: What's the best way to get into Gans for a Python learner ??
https://www.fast.ai/


nomorequity: How does go actually manage the repository imports?
Because you are import from "stylist" and your repo name is "stylist-enterprise" ?

go.mod

panic: ERROR #42P07 relation "colorschemes" already exists

:help :make
