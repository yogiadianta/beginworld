#2-9

Multi-selection demnu scripts
  -> Ya dummy

.gif
.png

!center alexa

---

https://askubuntu.com/questions/831331/failed-to-change-profile-to-headset-head-unit/1236379#1236379h,,

## Problem:
  - We already use |, to chain multiple commmands:
    -> however it runs each command seperate

  We need &

!rotz tswift 180 & scalex tswift 90

!sheery gopher 59 500
!norm gopher
!scale alexa 1
!normiememe gopher

!moveright gopher 200
!moveright gopher 200
!moveleft gopher 100
!moveup gopher 100
!scale gopher 2
!reset gopher
!moveup alexa 200
!moveleft alexa 200
!rotz alexa 359 2000
!fov alexa 0 2000
!fov alexa 300 200
!norm alexa
!posx alexa 200
!posy alexa -200
!sheerx alexa 200
!sheery alexa 200
!rotz alexa 200
!rotz gopher 200
!fov gopher 200
!fov gopher 0
!fov gopher 100
!fov gopher 200
!norm gopher 0.1
!hide gopher
!show gopher
!roty gopher 360 30
!rotx gopher 360 5
!rotz gopher 360

!norm gopher
!sheery gopher -190
!sheerx gopher -190
!sheery gopher -190 & !sheerx gopher -190
!posx gopher 0 & twsheery gopher -190 & !sheerx gopher -190
!sheery gopher -190 & !fov gopher 180


!reset gopher


---

!abszoom bmo 90
!saturate bmo 100
case "!sharp":

// When you have too long of a duration
// it just pauses and "snaps" to the final values

alex

!rotz tswift 180 20000
!normiememe tswift
!fall tswift
!scaley tswift 30000

!addmemefilters alexa
!move twice
!normiememe bmo
// this is short-enough
!rotz bmo 90 10000
!rotz bmo 90 20000

!moveright bmo 200
!moveup bmo 200

!sheery bmo 100
!positionx bmo 200

!fall gopher

!scale gopher 2
!moveleft gopher 300
!moveup gopher 300

!posx alexa 359
!posx alexa 359
!posy alexa 359
!roty alexa 180
!posz alexa -180
!norm alexa
!normiememe alexa

case "!fov":
case "!roty":
case "!rotx":
case "!rotz":
case "!sheerx":
case "!sheery":
case "!scalex":
case "!scaley":
case "!positionx":
case "!positiony":

mr_villescas: your thoughts of vimscript vs lua??
beginbot: ....I'm being influenced by fellow OnlyDev and Neovim core-developer
....so Vimscript is annoying and slow....and its annoying learning something
doesn't have other applications

I learn vimscrpit, I can make slow Vim plugins! Hooray!

...I learn Lua....I can make fast Vim Plugins hooray!
....and World or Warcraft mods
And Factorio mods
AND bunch of other
Roblox
minecraft Mods

## Begin Options

- Do we want to save all filter settings in our DB....
...I don't want to....but we might have to.


!scale technofroggo 0.3
!moveright technofroggo 300
!moveup technofroggo 200

!addmemefilters technofroggo
!rotx technofroggo 359 | !normiememe technofroggo | !fov technofroggo -180
!fov technofroggo 0
!normiememe technofroggo

---

The phrase “it’s just programming” is such a weak mindset. You are ok with what happened, losing, imperfection of a craft. When you stop getting angry after programming, you’ve lost twice. There’s always something to learn, and always room for improvement, never settle.


## Problems

- Qui-Gon Jinn broken downloading
- Back-alley chat not working


---

paytonrules: "Let's use a framework from a company we all hate on a website we can't stand" - beginbot

The -e flag changes the handling of erroneous packages, those that cannot be found or are malformed. By default, the list command prints an error to standard error for each erroneous package and omits the packages from consideration during the usual printing. With the -e flag, the list command never prints errors to standard error and instead processes the erroneous packages with the usual printing. Erroneous packages will have a non-empty ImportPath and a non-nil Error field; other information may or may not be missing (zeroed).

## Future Stream Collab

mdempsky: Wait, you're not a bot? I assumed you were AI-generated CGI.
beginbot and OnlyDevs: want to talk to you about Go compiler Work
....and the Drama

....I'm not interested in Drama for Dramas sake, but
this is interesting Drama, that relates to our lives as software devs

greatest_lord: @beginbot can you inherit go project from others developers that nobody wants to work with because of completely outdated dependencies, required environment? its common in javascript, i thought go is more strict on this
beginbot: it is entirely possible to do this in Go, it's probably less common
because Development communities (GO Standard Lib VS Left-Pad Bois)

## Coding Conversatoins

- What is from the langauge, what is from the community:
  - The problem of projects with outdated/unmaintained deps
    is always community

## Begin Goals

- We have a 3D Transform -> we allow users to update the values
  -> Then we hit a move transition to return to the OG values

## Viewer Questions

kobradre: My professor is a prick, tips on how to deal with him
beginbot: You will have to work with pricks and jerks your whole life.
  They don't all want to be pricks, many don't know they are.
  It's actually a sad tale. So be respectful to the sad situation,
  you know whats it like being mean to your students....takes
  a lot of darkness and sadness.
  But guess what you don't have to hang out with this person everyday
  for the rest of your life, so you just need to treat your interactions
  like the Gym. Welcome to the Prick Gym! Can you work as well as possible
  with this Prick. Now it hurts going to the Gym, its tiring,
  but soon your powers to deal with Pricks will be short-lived.
  You yourself will not become a jerk......heres the dumb mistake
  most people make: You're a Prick, I'll a prick....then someone new sees
  you and says that person's a prick, I guess I'll be a prick and
  cycle continues.

## Scraps


!viewer_filter alex 50 50 100 100 090
!normiememe alex

---


!copymemefitlers

!addmemefilters alex

!fall gopher
!fall  alex
!scale gopher 1
!normie gopher

!viewer_filter 0 0
!viewer_filter 50 50 100 100 090
x, y, xrotatin, yrotation, zrotation
!viewer_filter 0 0 100 100 090

!

  - The problem of projects with outdated/unmaintained deps
    is always community

3 Filters:
  -> normie_transform -> This is a move transition
  -> transform        -> 3D transform

  -> viewer_transform -> 3D transform
  -> viewer_move      -> move

## The Dream

- Allow viewers to control 3d Transforms:
  -> 3D Transform Filter
  -> Move Transition Filter

## Today

- Have Fun
- Learn Things
- Let Viewers Control Memes
  - more control of 3D Transform

## OBS Emotions

- When you have nested scenes:
  - You can't see the whats on the nested scenes, unless you are in "Studio Mode"


---


    -- We are going to try and Create a new image: quigontogglebit         !!!!! WE ARE in CreateMemes
Error Creating Default Meme: {ID:0 Name: X:0 Y:0 Scale:1 Rotation:0 PositionType:default Enabled:true Width:0 Height:0
MemeType:image Filename:} ERROR: duplicate key value violates unique constraint "memes_name_position_type_key" (SQLSTATE 23505)
Error Creating Current Meme: {ID:0 Name: X:0 Y:0 Scale:1 Rotation:0 PositionType:current Enabled:true Width:0 Height:0
MemeType:image Filename:} ERROR: duplicate key value violates unique constraint "memes_name_position_type_key" (SQLSTATE 23505)
[obsws] 2021/02/09 08:06:42 unmarshalling map -> *obsws.Response: '' expected type 'obsws.Response', got 'map[string]interface {}'
[obsws] 2021/02/09 08:06:42 input: map[string]interface {}{"error":"specified source doesn't exist", "message-id":"8618", "status":"error"}
Set Source Position quigontogglebit 0.000000 300.000000
[obsws] 2021/02/09 08:06:42 unmarshalling map -> *obsws.Response: '' expected type 'obsws.Response', got 'map[string]interface {}'
[obsws] 2021/02/09 08:06:42 input: map[string]interface {}{"error":"specified source doesn't exist", "message-id":"8625", "status":"error"}
[obsws] 2021/02/09 08:06:42 unmarshalling map -> *obsws.Response: '' expected type 'obsws.Response', got 'map[string]interface {}'
[obsws] 2021/02/09 08:06:42 input: map[string]interface {}{"error":"specified source doesn't exist", "message-id":"8626", "status":"error"}
[obsws] 2021/02/09 08:06:42 unmarshalling map -> *obsws.Response: '' expected type 'obsws.Response', got 'map[string]interface {}'
[obsws] 2021/02/09 08:06:42 input: map[string]interface {}{"error":"specified source doesn't exist", "message-id":"8627", "status":"error"}
        Sliding Source: quigontogglebit source, settings = map[end_x:1365 start_x:0 y:300]
WE ARE NOT CREATING A DEFAULT MEME

```

