# 2020-8-21

## Send the Elevator Back Down

We are developers
We are learning things
Going up the floors of knowledge and career
When you get up some floors,
its good to send the elevator back down for others

What does sending the elevator down look like?

- Helping a beginner
- hooking up someone for a job interview
- giving advice to someone

“If you have done well in whatever business you are in,
it’s your duty to send the elevator back down and try to help bring up the next generation of undiscovered talent.”
- erikdodev
  -Kevin Spacey

---

// The go vet tool checks that CancelFuncs are used on all
// control-flow paths.

// Do not store Contexts inside a struct type; instead, pass a Context
// explicitly to each function that needs it. The Context should be the first

// Do not pass a nil Context, even if a function permits it. Pass context.TODO
// if you are unsure about which Context to use.
---

## Day Sandwich

Do it before you open yuou computer, and after you shut it for the day
Sandwich of your day.
Do it for 5 ddays
See what happens

Simplest of Advice: Every Morning and Night take 10 mins

Morning:

- What I a grateful for?
- What would make today AWESOME (That I am in control of)

Night:

- What made today awesome?
- What could have made it better (that I am in control of?)
  - Any quick action items to fix this tomorrow

---

Talk Fast, Drive Fast, Live Fast - Code Slow

arschles: there's a usage example of the jaeger, datadog and stackdriver usage in a project I work on called Athens
arschles: https://github.com/gomods/athens
arschles: not trying to do a flex there lol. I didn't write the code, it was other contributors :)

arschles: opencensus is merged with opentracing, so names are interchangeable iirc
erikdotdev: Those are going to play the best with tools and services like Jaegar, etc
arschles: info on that here: https://medium.com/opentracing/a-roadmap-to-convergence-b074e5815289

- https://opentracing.io/docs/getting-started/

arschles: @endofunctorzero it depends on the way you're communicating over services
arschles: there's a built-in plugin for grpc iirc, if you're doing HTTP/JSON you either have to roll your own or pick up a distributed tracing library to do it for you
erikdotdev: Yeah and you can derive contexts from other contexts. So that you can set a timeout on the individual call, but a different timeout up at the parent.

arschles: two great justforfunc episodes (Go screencasts) on context also: https://www.youtube.com/watch?v=LSzR0VEraWw&list=PL64wiCrrxh4Jisi7OcCJIUpguV_f5jGnZ&index=36&t=0s and https://www.youtube.com/watch?v=8M90t0KvEDY&list=PL64wiCrrxh4Jisi7OcCJIUpguV_f5jGnZ&index=35&t=0s

https://podcasts.apple.com/us/podcast/rework/id1264193508?i=1000487783723

- DHH kicking up a storm against Apple
.....then Epic starts the same thing!
.....good ally

## Interviews are Dates NOT Quizzes

- when you interview, I keep a notepad, and I write questions
  the whole time, I also have some pre-questions already written down.


```
Plug 'nvim-lua/plenary.nvim'
teej_dv: local neorocks = require('plenary.neorocks')
teej_dv: neorocks.install('luasocket')
```

## Prepare for the Worst and Keep the Funnel Full to Stop the Depression

- Apply for a job, the day you do an you interview you like.
  Never just STOP! because you are excited about an opprotunity.
  There are lots of opprotunities! And if you keep the funnel full
  BOOM.

crypticdreamz8923: how do you identify a con artist during an interview?
beginbot: this is why you do an interveiw that is meant to find the edges
  of someones knowledge. So you have to fidns omething, you know about,
  that they don't and to be to see if they are bullshitting.

It's WAAAY Easier for a frontend dev to con a backend dev in an interview,
and this applies in all directions.

## Interviews that could happen

....the interview might be the goal of breaking you.
 find what you don't know
 find wht happens when you're pushed a little
 find what happens in a zone you're not comforatble wtih

 ...the questions, could literally, be moving, to find something
 YOU DON'T KNOW!!!!!

 THE GAME IS NOT KNOWING EVERYTHING, interviewer wants to see
 what you're like at the edges of your knowledge, or how
 your ego interacts with not knowing things and collaborting.

## Interviewer Belief

- If theres something I could study for an hour before the interview
  and pass.....then THAT'S TRIVIA....and  we are want to make the greatest
  Jeopardy coding focused team, then hell yeah

erikdotdev: Don't beat yourself up if you don't pass.
They have a short window of time to evaluate abilities that take hours, weeks,
months to actually use in a meaningful way.

Even the BEST people get turned down.

Interviewing takes some warming up or practicing:

- Tell us a hard a time, and how you overcame it?
- Tell us about some code you were particular proud about?
- Tell us about a time, where you broke something, and how'd you resolve
  it?

....by the time you answered 10 times....they sound better.

....even the best people freeze up in interviews.

Performing IS ALWAYS DIFFERENT than Practicing
Nothing beats much performances

## Begin Stream Concepts

- I like to present the counter-narrative:
  What I hear: Everyone obsess with typing speed
    WPM, keyboards "Efficency", learnign something ""fast"
  What I want hear: Everyone obsess over what they are learning
    what is confusing them, what they are enjoying

Beginners are obsessed with all the wrong stuff: Typing in Programming being
often top of the list

teej_dv: Kappa
Senior Scam: Convince all beginners that typing speed is the most important
factor in programming....THEN THEY WILL NEVER LEARN TO PROGRAM
THEN TEEJ IS THE ONLY PROGRAMMER LEFT
Job Security!!!

If we convince new programmers, that programming is just creating algorithims
from text books, in timed settings -> NO ONE WILL WANT PROGRAM

convince programmers, that your code can scored
if we convince programmers, that code can be graded, and ranked
WE CAN PRETEND IT'S VIDEO GAMESSSSS!!!!!!
THEN WE CAN GET PEOPLE ADDICTED TO A NEW TYPE OF VIDEO GAME
....while convinving them they are programming.

GPT-3 videos -> Just give up, it's over. No reason learning to program
AI will do it all.

In the future -> Only programmers who can type 300+ WPM will be able to keep
with the AI

Soon it will just be the tiny-elite like Teej, driving around
in their Lambos, writing the only non-AI code.

0.0000!% of Programmers

10X

0.000001% Programmers

erikdotdev: Only AI and neovim users beginbTeej

zanuss: You sell them a keyboard in 4 parts over the 4 years, they have to master one part before they get the next one

## Password Tips

I like just gibberish statements to future begin

## Work on New Sterotypes

- Woah you drive a lambo??? Wait do you write text editors???

---

https://blog.labix.org/2011/10/09/death-of-goroutines-under-control

```
function! AllMap(lhs, rhs) abort
  execute 'nnoremap ' . a:lhs . ' ' . a:rhs
  execute 'vnoremap ' . a:lhs . ' ' . a:rhs
  execute 'inoremap ' . a:lhs . ' ' . a:rhs

  " etc.
endfunction

call AllMap('<up>', '<nop>')
```


- https://google.github.io/flatbuffers/

## Resources

https://tom.preston-werner.com/2010/08/23/readme-driven-development.html

## Go Conundrum

We have go routines, launching goroutines
What do we want to do, if a parent goroutine is shutdown, cancelled?
  - How do we tell the child that we are done.

- if I launch a Child Go Routine, I am always passing
  a done chan, so I can cancel it

erikdotdev: If you think of it in the context of an http request it makes a lot of sense.
HTTP request comes in, you fan out to do a bunch of work.
One of the async functions failed. You want a way to cancel all the other async calls,
or if one of them times out.
You also want a way to cancel anything goroutines those calls have spun up

- context.Context can used in a single app, AND ACROSS APPPS!!!!

erikdotdev: yeah, if you use things like gRPC you can pass the Context across the network to other services

## Today

-  when would I use sync/atomic versus "higher-level" syncronization
  primitives like channels and whatnot?
  - skippednote: multiple goroutines trying to update a common variable.

erikdotdev: If you really want the juicy details on the theory behind them. You can read the paper they are based off of:
Communicating Sequential Processes by Tony Hoare

erikdotdev: For other geeky details. Go's channel inplementation are basically borrowed from another language that Rob Pike worked on. Newsqueak

https://paperswelove.org/

- Learn more about Go testing

## Go Opinions

- https://golangdocs.com/the-test-functions-in-golang
  - How do Gophers feel about created this "extra" functions
    for cleaner tests, in each project?
      is this what I testify is helpful for???

- Are there testify people and others who say DIY???

- People don't recommend testify just for all projects

erikdotdev: Your hardcore Go folks roll their own, that's the direct answer to
your question. Like I don't see the Go team using external libraries, they build
their own helpers
erikdotdev: but in the ecosystem, you will see people in the community use a lot of the other tools you see, testify probably being the most popular

Different communities have different conventions around interacting
with "Third-Party" packages. It would probably be very frustrating
to tell a new JS dev, to not use 3rd party packages.

erikdotdev: Yeah, the Go community does try to reduce the number of dependencies for things when possible. Borrow the function that you need from the OSS project vs importing the entire lib, etc.

## Go Testing

TestMain is used for test teardown and setup

```
func TestMain(m *testing.M) {
	// call flag.Parse() here if TestMain uses flags
	os.Exit(m.Run())
}
```

erikdotdev: @beginbot Did you see my comment above about the fact that the thing you're really repeating is usually at a much higher level than checking a string, so you're really rolling your own anyway?
