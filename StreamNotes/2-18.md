# 2-18

viewer_move filter: move-transistion OBS plugin provided filter
WAS ON
....it's only supposed to stay on while executing:
  1000 ms -> 1 second

## Bugs

- Why can't otter fall?
  -> otter is on the outfits scene
  -> my code is litered with the constant: MemeScene
    - there was only one meme scene
    - but not anymore
    ...if fall works on some Memes and not others
    ...it depends on the MemeScene

!fall otter

## BeginWorld split needs to happen;

- Separate all OBS commands to read off of DB messages
  -  Then we can run Audio and OBS bot separately

## Viewer Questions

asbjornhaland: @beginbot you are a telescope user. Have you noticed that if you leave 'insert mode' then try to reenter insert mode, you can't delete the text. Same at your end or any idea how to fix?

Hello Im in insert mode

asbjornhaland: telescope have a insert and normal mode. if you esc insert mode INSIDE telescope, you can j/k etc
sniffaless: hi
punchypenguin: my #1 hero

sniffaless: I have a question.

I will get into coding for about 2 months at a time create a project such as the website I made.

But then I don't code for 6 months because I cant come up with projects and suggestions?

BeginBot advice:

- you code a project...then you abandon it
  ...why...
  ...the reason why is there are not no consequences for abandoning the project.

You need to build in consequences:
  - Launch date you are building up to?
  - some sort of public presenting of your project?
  - some sort of deadline, that if you don't hit it
  ....someone will yell at you
  ....or at least say: I'm a little dissapointed


!move quiGonTogglebit 0 400
!attac

!move quiGonTogglebit 0 400

!move qui
!move quiGonTogglebit 0 -100
!move quiGonTogglebit 0 100
!move quiGonTogglebit 700 400
!move quiGonTogglebit 700 400
!move quiGonTogglebit 0 400
!move quiGonTogglebit 0 600
!move quiGonTogglebit 0 1000
!move quiGonTogglebit 0 201


!normie begin
!reset quiGonTogglebit



---

stupac62: have you seen this? https://pkg.go.dev/golang.org/x/mobile/exp/sprite if you go down the sprite path
punchypenguin: @Sniffaless more website
anafromain: @Sniffaless at least you make a project! I think of one and go learn a new programming language instead and get stuck in tutorial hell
dardub: @sniffaless a good way to practice is just copy things you see that you like
punchypenguin: @Sniffaless preferably to do with building more projects
stupac62: @Sniffaless Copy other peoples ideas. There are thousands of people posting things such as "programming projects"

## BeginWorld Paper work submitted

- 2 Lifetime Subs -> Partner:
  -> zanuss
  -> stupac62

!show otter
!hide otter
!fall otter

## What's New in Beginworld

- We are saving Scenes for Memes in the DB
  -> this means that we can divide the scenes we store Memes on:

- viewer_memes -> default catch-all
- outfits
- backgrounds_scene
- pokemon_scene not filled

!memetriforce

## Today

- More control of Memes
  - Pull things into more scenes
    -> Fix Grilling

## Warm-Up

- Lets get all these flipping once
