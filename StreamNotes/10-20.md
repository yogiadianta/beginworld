# 10-20

## Life Changing News

- You don't need to start a company, to build software
  to help other developers. You can do it right now,
  for free.

It's Called Open Source

janpoonthong: I want to have a company that improve developers with software, it
could be small things like linters, automate web stuff, etc...

Find a problem developers have you want to fix, fix it now!

## Quotes

rexroof: the real startup were the friends we met along the way

## Ruby Days

- Sandys Rulez

- Classes less than 100 lines
- Functions less than 5 lines
- no more than 3 args (maps count as 3)
- Rails specific: only pass one instance variable from controller to view

## Bug Format

Problem:
Theory:

## Bugs

Problem: Themes played for users every time
Theory: Not saving/looking for chat messages in the same place

Theory 2: It has o

Real Life Error:

NewSave Error = &{Config:0xc000032b80 Error:<nil> RowsAffected:1 Statement:0xc000416000 clone:0}


## Setup

make test

---

## Scraps

```
err = &{Config:0xc00014cb00 Error:<nil> RowsAffected:1 Statement:0xc000124000 clone:0}
New Haven't Chatted Today:  bopojoe_


Did bopojoe_ Chat Today: false
Fetching From Folder:  theme_songs
Searching Within 2937 Samples
Match Found!  bopojoe_
        Audio Authorizer:       Authorizer: Allowed: true | Streamlord: false | Mana: 4 | Owned: true
        Saving OG AudioRequest:         Authorizer: Allowed: true | Streamlord: false | Mana: 4 | Owned: true
Err Saving Audio Request:  &{0xc00014cb00 <nil> 1 0xc000124000 0}
Adding a Theme Song to the Queue:  bopojoe_
Stream Lord Checkin:  AudioRequest<297 bopojoe_ bopojoe_ Streamlord: false>
Marking Sound as Played: bopojoe_
```

