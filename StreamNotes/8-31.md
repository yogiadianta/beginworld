2020-8-31

## Questions

stupac62: aren't fads only identifiable in retrospect?

- You must build nvim-clippy

## Begin's Lies to Himself

Lie: Yeah i'll code all night, and get sooo much done!
Truth: You make up early, do all the hardest work while the light it out,
and you're fully awake. Then at night, you go to sleep.

- https://github.com/nvim-lua/telescope.nvim

- gr -> references under cursor
- CTRL-p -> fuzzy find files
- CTRL-g -> grep
- CTRL-k -> Hover Definition
- <leader>gd -> goto definition

```
# Teej Style
nnoremap <C-g> :lua require('telescope.builtin').live_grep{}<CR>

nnoremap <C-p> :lua require'telescope.builtin'.git_files{}<CR>

nnoremap <silent> gr         <cmd>lua require'telescope.builtin'.lsp_references{}<CR>
nnoremap <silent> <c-k>      <cmd> lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> gd         <cmd> lua vim.lsp.buf.declaration()<CR>
```

Columns must be 120 at least to get preview for Telescope:
  - How it easy to see columns all the time, or flash Them, or note 
  on the screen when the font is too big to pull it up

ctrl-k
then ctrl-k to close this happened
```
Error executing vim.schedule lua callback: /usr/local/share/nvim/runtime/lua/vim/lsp/util.lua:673: Failed to switch to window 1014
```

If I just move off, it doesn't throw an error
