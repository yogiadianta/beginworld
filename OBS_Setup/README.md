# OBS Setup

This is mostly for me right now, but as we distribute Beginworld more, this will be important.

## Scenes

### Primary

Main Scene Begin streams from

## Sources

BeginCam      - Begin's main camera
Mic           - Begin's Mic
ComputerMusic - Audio From Computer
Screen        - Main Screen Begin is Looking at
twitchchat    - Streamlabs Twitch Chat

## Every Meme

Every Meme has a number of filters:

- Transform3D:
  -> This triggers 3D effects

- MoveTransform3D
  -> This transistions into 3D transform Effects

- MoveSourceHome(Name)
  -> This is on the scene, and allows moving to its home or default location
  -> we have this mapped to Super+L

- MoveSource(Name)
  -> This is on the scene, and allows moving the source all around the scene
  -> we have this mapped to Super+H

### Color Codes

Red       - Cameras
Yellow    - Microphones
LightBlue - Notifications
DarkBlue  - Backgrounds
Green     - Sound
Magenta   - Memes

