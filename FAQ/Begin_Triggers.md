# FAQ/Begin_Triggers.md

## Triggers

- Apologizing for Questions
  - There are NO stupid questions
  - only stupid people

- Give me a Project
  BEGIN: This NEVER WORKS
         The Ability to find/plan your own project
         is the skill you need
         https://gitlab.com/beginbot/beginworld/-/blob/master/FAQ/Give_Me_A_Project.md

- I want to learn Data-Structure and Algorithms, but not for any end goal
  BEGIN: What are you trying to do
         Work backwards
         What kind of programming
         What kind of jobs


