# Writing Your First Blog Post

## Where to Host

- [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Github Pages](https://pages.github.com/)
- Notion.so
- Netifly

## Prerequisites

- Take notes on all your learning
- Have a Github and/or Gitlab

## Questions to Ask Yourself

Question: What do I write about?

### Whatever you learned that Week

- Who cares if it isn't a unique post.
- Example accurately what is the most frustrating thing you're experiencing
- Write about what you failed at
- Write about one thing you were confused about and now understand
- Refer to your notes, extract out anything you were confused about, and blog
on it

### Something unique to you

Example: AdrnlJnky is a master of the River.

Question: How do I find what I interested in?

More Questions:

[Flow State](https://www.wikiwand.com/en/Flow_(psychology))

- What things make you slip into Flow State doing?
- Make you forget the time
- Make you wake up early and do right away
- You can talk endless endless to your friends

#### Basic Categories

- Physical Activity
- Video Games
- Cooking
- Music
- Reading

#### Physical Activity

- Team Sports
- Technical Sports
- Do you like solo activities
- Do you logistics in sports

#### Video Games

What kind of video games, and when do into flow:

- Incredible organizers in games
- Some people are good under pressure sitautions, spamming a million commands
- Some people like world building
- Some people love them stats
- Love deep puzzles games: distrubted systems programming

---

## Meta Idea

If you want to go from 0 to job:

- Producing Blogs
- Doing a talk (or recording it)
- Doing some complete smaller projects
- Open Source Contribution
- Be apart of Community (Discord, Language, Framework, Libray, Meetup)

What this is not:

- Not a Set Path, This is all about, finding what
  you are interested in, uniquely skilled at.

## Extra Discussion

rockerboo: i can't play puzzle games. I always be like why does it matter in
the game vs making my own systems and figuring out solutions to them.
