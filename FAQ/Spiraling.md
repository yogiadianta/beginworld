# Spiraling

What do you do, when you can't even understand the Docs/Manual???

- Read each word, very carefully,
  every single thing you don't understand,
  write down in question what you are confused about.
  When you collect 5.

  Start with the First, and the same process.
  Once You reach 5 again, go down another level.

  Decide when you want to come back up

  ## Open Questinos

  - What are Unix dialects
  - What are all the "Unix Dialects"
  - What is process in this case

  - What makes a file regular?
  - Whats a block special file?
   - a character special file,
   - an executing text reference

## Examples

     An open file may be a regular file, a directory, a block special file, a character special file, an executing text reference, a library,
     a stream or a network file (Internet socket, NFS file or UNIX domain socket.)  A specific file or all the files in a file system may  be
     selected by path.

     Instead  of a formatted display, lsof will produce output that can be parsed by other programs.  See the -F, option description, and the
     OUTPUT FOR OTHER PROGRAMS section for more information.

     In addition to producing a single output list, lsof will run in repeat mode.  In repeat mode it will produce output, delay, then  repeat
     the output operation until stopped with an interrupt or quit signal.  See the +|-r [t[m<fmt>]] option description for more information.

OPTIONS
     In the absence of any options, lsof lists all open files belonging to all active processes.

     If  any  list request option is specified, other list requests must be specifically requested - e.g., if -U is specified for the listing
     of UNIX socket files, NFS files won't be listed unless -N is also specified; or if a user list is specified with the -u option, UNIX do-
     main socket files, belonging to users not in the list, won't be listed unless the -U option is also specified.

     Normally  list  options  that  are specifically stated are ORed - i.e., specifying the -i option without an address and the -ufoo option
     produces a listing of all network files OR files belonging to processes owned by user ``foo''.  The exceptions are:

     1) the `^' (negated) login name or user ID (UID), specified with the -u option;

     2) the `^' (negated) process ID (PID), specified with the -p option;

     3) the `^' (negated) process group ID (PGID), specified with the -g option;

     4) the `^' (negated) command, specified with the -c option;

     5) the (`^') negated TCP or UDP protocol state names, specified with the -s [p:s] option.

     Since they represent exclusions, they are applied without ORing or ANDing and take effect before any other selection  criteria  are  ap-
     plied.

 ---

     Through the 1990s, UNIX continued to evolve and gain popularity as UNIX servers moved into mainstream data processing.  Many different unique dialects of UNIX have developed, each unique to each UNIX vendor, and today's Oracle professional must be fluent in many different dialects (often called flavors) of UNIX. With the demise of DEC (Digital Equipment Corporation) and its VMS operating system, Unix and its various flavors have become the dominant operating systems for the Oracle software.


One of the biggest problems for the Oracle DBA who wishes to work in the UNIX environment is that there has never been a single, unified UNIX product with total compatibility from one system to another. Most differences have arisen from different versions developed by three major early UNIX dialects - AT&T UNIX, Berkeley's BSD UNIX and Microsoft's XENIX product.  All of these Unix flavors are similar, but no two are exactly the same.

 

Today, the most popular dialects of UNIX include Hewlett-Packard UNIX (HPUX), IBM's UNIX (AIX), Sun UNIX (Solaris), and the popular Linux dialects (Red Hat Linux, SUSE Linux). This book is geared toward all versions of Oracle8i and Oracle9i, Oracle 10g, and Oracle 11g, and included are dialects of UNIX like HP/UX, IBM's AIX, Sun's Solaris, and Oracle's unbreakable Linux. Some commands in IRIX, DEC-UNIX and UNIXWARE will also be shown.
