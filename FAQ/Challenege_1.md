# First Blog Post Challenge

## 5 Participants

- tscheer100 / TheTurtleKing
- faolan
- Werrdd
- punchypenguin
- adrnlnjnky

## Topic Options

tscheer100 / TheTurtleKing:

- data visualization in python using COVID-19 data (Begin: this seems simpler,
  and finishable)
- personal dropbox (This seems like a much bigger project. Could turn into a
  serious of posts)

adrnlnjnky:

- Starting my first game - based off my job on the river!
- My intro to Vim

## Determining a Topic

What are you learning this week?

What is the last problem that you were stuck on programming?

What is a tip you wish you had known last week/month?

What is something you tried to learn and fail.
