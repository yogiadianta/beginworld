FAQ/Testing_Talk.md


## Things to Consider about Testing

- Types of Tests
  - Unit
  - Functional
  - Integration
  - Acceptance
  - Performance
  - Smoke
- Fragileness of Tests
  - When do you have to update the tests, when updating
    code that shouldn't be affecting it
- When are tests run in a development cycle
  - Writing the Code
  - Pushing to CI
  - As part of QA
  - Pre-releasing to an environment
  - Run once deployed to an environment
- Where tests are run
  - on a users computer
  - in CI
  - in an environment


## Questions to Ask Yourself

- When this test fails, what does that indicate to me
- When this test pass, what does that indicate to me
