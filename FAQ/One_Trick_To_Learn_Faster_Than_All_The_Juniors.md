# One Trick they don't want you knowing

Accept radical beginnerness:

- I do not care if I don't know things
- If people know things that I don't know, I'm going to ask
- I will assume every person is infinitely smarter than me,
  and has tons of good helpful value.

Example:

2 Juniors start on the same day:

Junior 1: Very indepdent, and wants to be a CODE Master,
          wants to be a super elite awesome coder. Wants to
          learn everything.
          ....afraid to admit what they don't know
          ....afraid to ask mentoring from the seniors
          ....hard to collaboarte with, because they want
              do it all themselves

Junior 2: Excited to Learn, Humble Beginner. Asks for
          help when needed, and is real open to the advice
          and mentorship of Peers/Seniors. Fun to work with,
          job not a competion, collabing.

Seniors love pairing or helping Junior 2, you end getting all
good info.

## How do we cultivate the Humble Beginner Junior Mindset

errwip: I always feel like I'm bothering people when asking for simple stuff tho
LUL

Why do you think that?
Whats the worst that could happen?
How can you ensure you are making the experience less bothersome?

Have steps you take before you reach for help:

- Always take notes
  - What you've done
  - You're Assumptions
  - What you're confused about
- Do a basic on the internet
  - Read the Manual (while taking notes!)
  - Read the full StackOverflow Question (while taking notes!)
  - Read the full Blogpost (while taking notes!)
- Try to solve it for a set amount of time;
  - Talk to your team, about what's good amount of time to be stuck.
  - 1 hour
  - 2 hours

When you reach out to the "senior":

you should have some concrete questions to ask.

The Core of What Annoys Aasking a Senior for Help:

- Doing your job

---

Begin Experience

I'm debugging
I'm typing
I'm SOing
I'm copying pasting
I'm spazzing out

Senior comes over and SAYS NO!!!!!!!

What are trying to do
What is your theory of whats going wrong
How you going test that
Test That
Repeat

YOU SOLVE BUGS WILL LIGHTNING EFFICIENCY and you have docs or runbook
afterwords.

The more you ok with looking dumb, the faster you'll the learn, the faster
you'll provide value. We must drop our egos, to learn best.
